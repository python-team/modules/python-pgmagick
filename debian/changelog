python-pgmagick (0.7.6-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ TANIGUCHI Takaki ]
  * New upstream version 0.7.6
  * Bump Standards-Version to 4.6.0
  * Bump debian-compat to 13
  * d/control: update Homepage URL

 -- TANIGUCHI Takaki <takaki@debian.org>  Sat, 11 Sep 2021 22:53:20 +0900

python-pgmagick (0.7.5-1) unstable; urgency=medium

  * d/gbp.conf: adjust branch structure
  * New upstream version 0.7.5
  * Bump debhelper from old 11 to 12.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Update standards version to 4.1.5, no changes needed.
  * d/salsa-ci.yml: Add a CI config file.
  * Bump Standards-Version to 4.5.0

 -- TANIGUCHI Takaki <takaki@debian.org>  Tue, 11 Feb 2020 11:40:00 +0900

python-pgmagick (0.7.4-3) unstable; urgency=medium

  * Team upload.
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support.

 -- Ondřej Nový <onovy@debian.org>  Sat, 24 Aug 2019 23:07:28 +0200

python-pgmagick (0.7.4-2) unstable; urgency=medium

  * Team upload
  * remove myself from uploaders

  [ Ondřej Nový ]
  * Convert git repository from git-dpm to gbp layout
  * d/changelog: Remove trailing whitespaces

 -- W. Martin Borgert <debacle@debian.org>  Sun, 10 Feb 2019 18:00:18 +0000

python-pgmagick (0.7.4-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/watch: Use https protocol
  * d/changelog: Remove trailing whitespaces
  * d/control: Remove trailing whitespaces

  [ TANIGUCHI Takaki ]
  * New upstream version 0.7.4.
  * Bump Standards-Version to 4.1.4.

 -- TANIGUCHI Takaki <takaki@debian.org>  Mon, 14 May 2018 21:58:51 +0900

python-pgmagick (0.7.2-1) unstable; urgency=medium

  [ TANIGUCHI Takaki ]
  * New upstream version 0.7.2
    - Fix "Image.attribute does not set the attribute but instead appends
    to it" (Closes: #690942)
  * debian/compat: 11
  * Bump Standards-Vertion to 4.1.3
  * debian/rules: Change Priority extra to optional

 -- TANIGUCHI Takaki <takaki@debian.org>  Sun, 04 Feb 2018 00:06:21 +0900

python-pgmagick (0.6.5-1) unstable; urgency=medium

  * New upstream version 0.6.5
  * Bump Standards-Version to 4.0.0 (without changes).
  * debian/compat: Bump to 9

 -- TANIGUCHI Takaki <takaki@debian.org>  Mon, 26 Jun 2017 21:06:19 +0900

python-pgmagick (0.6.4-1) unstable; urgency=medium

  * New upstream release

 -- W. Martin Borgert <debacle@debian.org>  Sun, 22 Jan 2017 13:45:47 +0000

python-pgmagick (0.6.2-2) unstable; urgency=medium

  * Update upstream homepage
  * Use debhelper compat 9

 -- W. Martin Borgert <debacle@debian.org>  Sun, 25 Dec 2016 20:29:19 +0000

python-pgmagick (0.6.2-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed homepage (https)
  * Fixed VCS URL (https)

  [ W. Martin Borgert ]
  * New upstream release (Closes: #825926, apparently)
  * Dropped patch, replaced with debian/clean
  * New standards version, no change
  * Added myself to uploaders

 -- W. Martin Borgert <debacle@debian.org>  Mon, 04 Jul 2016 22:13:18 +0000

python-pgmagick (0.6-1) unstable; urgency=medium

  * Imported Upstream version 0.6
  * Support Python-3 (Closes: #782948)

 -- TANIGUCHI Takaki <takaki@debian.org>  Thu, 26 Nov 2015 11:43:30 +0900

python-pgmagick (0.5.12-1) unstable; urgency=medium

  * Team upload.

  [ Sebastian Ramacher ]
  * New upstream release.
    - Fix build against graphicsmagick 1.3.22. (Closes: #804323)
  * debian/control: Add dh-python to B-D.

  [ SVN-Git Migration ]
  * Update Vcs fields for git migration.

 -- Sebastian Ramacher <sramacher@debian.org>  Sun, 08 Nov 2015 15:16:42 +0100

python-pgmagick (0.5.11-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * debian/watch: Use pypi.debian.net redirector.

 -- Sebastian Ramacher <sramacher@debian.org>  Thu, 13 Aug 2015 23:03:06 +0200

python-pgmagick (0.5.8-1) unstable; urgency=medium

  * New upstream release
  * Bump Standards-Version to 3.9.6

 -- TANIGUCHI Takaki <takaki@debian.org>  Wed, 05 Nov 2014 10:23:52 +0900

python-pgmagick (0.5.7-1) unstable; urgency=low

  * New upstream release
  * Bump Standards-Version to 3.9.4.

 -- TANIGUCHI Takaki <takaki@debian.org>  Fri, 06 Sep 2013 10:51:55 +0900

python-pgmagick (0.5.6-1) unstable; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ TANIGUCHI Takaki ]
  * New upstream release

 -- TANIGUCHI Takaki <takaki@debian.org>  Wed, 08 May 2013 19:47:58 +0900

python-pgmagick (0.5.1-1) unstable; urgency=low

  * New upstream release
  * Bump standards version to 3.9.3.
    + Update debian/copyright URI.

 -- TANIGUCHI Takaki <takaki@debian.org>  Mon, 27 Feb 2012 09:51:07 +0900

python-pgmagick (0.4.2-1) unstable; urgency=low

  [ Jakub Wilk ]
  * Add Vcs-* fields.

  [ TANIGUCHI Takaki ]
  * New upstream release
  * debian/control: Update description. (Closes: #638362)

 -- TANIGUCHI Takaki <takaki@debian.org>  Tue, 20 Sep 2011 11:07:32 +0900

python-pgmagick (0.4.1-1) unstable; urgency=low

  * New upstream release

 -- TANIGUCHI Takaki <takaki@debian.org>  Fri, 05 Aug 2011 11:19:40 +0900

python-pgmagick (0.3.6-1) unstable; urgency=low

  * New upstream release
  * copyright: DEP-5

 -- TANIGUCHI Takaki <takaki@debian.org>  Mon, 20 Jun 2011 10:51:29 +0900

python-pgmagick (0.3.5-1) unstable; urgency=low

  * New upstream release
  * Bump Standards-Version to 3.9.2 (with no changes).
  * Swtch to dh_python2. (Closes: #625853)

 -- TANIGUCHI Takaki <takaki@debian.org>  Sat, 07 May 2011 10:57:58 +0900

python-pgmagick (0.3.3-1) unstable; urgency=low

  * New upstream release

 -- TANIGUCHI Takaki <takaki@debian.org>  Tue, 05 Apr 2011 23:54:58 +0900

python-pgmagick (0.3.2-1) unstable; urgency=low

  * New upstream release
  * debian/patches/00_fix_makefile: removed

 -- TANIGUCHI Takaki <takaki@debian.org>  Fri, 04 Feb 2011 13:25:01 +0900

python-pgmagick (0.3.0-1) unstable; urgency=low

  * Initial release (Closes: #601047)

 -- TANIGUCHI Takaki <takaki@debian.org>  Mon, 15 Nov 2010 21:27:13 +0900
